const numberRangeOfValue = (number) => {

let numberString = number.toString();
    let cyphers = numberString.split("");
    let visionValues = [];
    for (i = 0; i < cyphers.length; i++) {
        currentCypher = cyphers[i];
        visionValues[i] = 0;
        let visionRange = currentCypher;
        let leftCounter = 1;
        let rightCounter = 1;
        let noMoreLeft = false;
        let noMoreRight = false;
        while (visionRange > 0) {
            leftValue = cyphers[i - leftCounter];
            rightValue = cyphers[i + rightCounter];
            if (typeof(leftValue) === "undefined") {
                noMoreLeft = true;
            }
            if (typeof(rightValue) === "undefined") {
                noMoreRight = true;
            }

            if (noMoreLeft && noMoreRight) {
                break;
            }

            if (noMoreRight) {
                visionValues[i] += parseInt(leftValue);
                leftCounter++;
                visionRange--;
            }

            if (noMoreLeft) {
                visionValues[i] += parseInt(rightValue);
                rightCounter++;
                visionRange--;
            }

            if (!noMoreRight && !noMoreLeft) {
                if (leftValue < rightValue) {
                    visionValues[i] += parseInt(leftValue);
                    leftCounter++;
                    visionRange--;
                } else {
                    visionValues[i] += parseInt(rightValue);
                    rightCounter++;
                    visionRange--;
                }
            }

        } 
    }
    
    let minimumVisionValue = Math.min(...visionValues);
    let indexOfMinimumVisionValue = visionValues.indexOf(minimumVisionValue);
    let indexOfTheOneCypher = cyphers.indexOf('1');
    if (indexOfTheOneCypher === indexOfMinimumVisionValue) {
        return true;
    }
    return false;

}

console.log(numberRangeOfValue(733123));
